"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

    Challenge 2.: Appending Value to the End of a List
Given an AppendtoList() function, create a list named l with the following values:  [1, 4, 9, 10, 23]
and appends the number 90 at the end of the list.

    Input   #A list of numbers
    
    Output  #Append the value 90 to the end of the list l
    
    Sample Input:
    >[1, 4, 9, 10, 23]
    
    Sample Output:
    >[1, 4, 9, 10, 23, 90]

"""
def AppendtoList():
    l = [1, 4, 9, 10, 23]
    l.append(90)
    return l        #l + [90] works too

if __name__ == '__main__':
    print(AppendtoList())