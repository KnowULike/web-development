#######################################################################################################################
### Subject: Python                     ###############################################################################
### Author: António Francisco C. Rocha  ###############################################################################
#######################################################################################################################

#       Challenge 8: Sum of Squares of Even Numbers
#   Given an evenSquare() function, create a list with the squares of the even numbers from 0 to 20. The final output
#   should be the sum of even numbers in the list:

#     Input:  A list with the square of even numbers from 0-20

#     Output: The sum of the numbers in the list

def evenSquare():
    return sum([x ** 2 for x in range(0, 21, 2)])   #List omitted for simplification purposes


if __name__ == '__main__':
    print(evenSquare())
