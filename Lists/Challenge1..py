"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

       Challenge 1.: Sublist of a List
Given a getSublist() function, create a list named l [1, 4, 9, 10, 23]. Using list slicing, get the sublists [1, 4, 9] and [10, 23].

Input   #A list
    
Output  #Two sublists

"""
def getSublist():
    l = [1, 4, 9, 10, 23]
    l1 = l[:3]
    l2 = l[3:]
    return f"{l1}\n{l2}"


if __name__ == '__main__':
    print(getSublist())