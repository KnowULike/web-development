#Subject: Python
#Author: António Francisco C. Rocha
##############################################################################################

#        Challenge 7.: Lists of Even and Odd Numbers
# Given a ListofEvenOdds() function, create a list comprehension with all the even numbers from 0 to 20, and
# another one with all the odd numbers.
#
# Input: Two empty lists
#
# Output:
#     >List 1 with even numbers
#     >List 2 with odd numbers
    

def listOfEvenOdds():
    evenNumbers = [x for x in range(1, 21) if x % 2 == 0]           #Could change range to get even and odd numbers
    print(evenNumbers)
    oddNumbers = [x for x in range(1, 21) if x not in evenNumbers]
    print(oddNumbers)
    return

if __name__ == '__main__':
    listOfEvenOdds()