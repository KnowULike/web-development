"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

       Challenge 6.: List of Cubes
Given a getCube() function, create a list with the cubes of the first 20 numbers.

    Input #An empty list
    
    Output #An updated list with the cube of each value in the list

"""
def getCube():
    return [x**3 for x in range(1, 21)]

if __name__ == '__main__':
    print(getCube())