"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

      Challenge 4.: Remove Sublist From List
Given a removeFromList() function, create a list named l with the following values:     [1, 4, 9, 10, 23]
Remove the sublist [4, 9] from list l
    
    Input       #A list
    
    Output      #The updated list after the sublist has been removed
    
    Sample Input:
    >[1, 4, 9, 10, 23]
    
    Sample Output:
    >[1, 10, 23] 

"""
def removeFromList():
    l = [1, 4, 9, 10, 23]
    subL = [4, 9]
    for e in subL:
        l.remove(e)
    return print(l)

if __name__ == '__main__':
    removeFromList()