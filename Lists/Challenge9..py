#######################################################################################################################
### Subject: Python                     ###############################################################################
### Author: António Francisco C. Rocha  ###############################################################################
#######################################################################################################################

#     Challenge 9: Even Squares Not Divisible By Three
# Given a getSquare() function, make a list comprehension that returns a list with the squares of all even numbers from
# 0 to 20, but ignores those numbers that are divisible by 3.

#   Input: An empty list

#   Output: A list with the square of all even numbers not divisible by 3.

def getSquare():
    return [x**2 for x in range(0,21,2) if x % 3 != 0]

if __name__ == '__main__':
    print(getSquare())