"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

       Challenge 5.: List of Squares
Given a getSquare() function, create a list with the squares of the first 10 numbers, i.e., in the range from 1-10.

Input #An empty list

Output #An updated list with the square of each value in the list

"""
def getSquare():
    l = [x*x for x in range(1, 11)]
    return l

if __name__ == '__main__':
    print(getSquare())