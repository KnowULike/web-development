# ---------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

    #   Challenge 0: Create a Module
#A function is a block of code that is used to perform a single action. A module is a Python file
# containing a set of functions and variables of all types (arrays, dictionaries, objects, etc.)
# that you want to use in your application.

# To create a module, create a python file with a .py extension.

# How to use: #Modules created with a .py extension can be used with an import statement

def returnString():
    return "This is a string returned via import!"  #Results in the next challenge!