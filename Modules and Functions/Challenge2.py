# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------
#
#      Challenge 2: Calculate Sine, Cosine, and Tangent of User Input
# Use the calculateSinCosTan() function; it takes a number x as a parameter and shows the result of the sine,
# cosine, and tangent of the number.
#
#     Input: A number
#
#     Output: Calculate the sine, cosine, and tangent of that number
#
#     Sample Input:
#         >0
#
#     Sample Output:
#         >[0, 1, 0]

import math

def calculateSinCosTan(x):
    return [math.sin(x), math.cos(x), math.tan(x)]      #[sin, cos, tan]

if __name__ == '__main__':
    print(calculateSinCosTan(-1))
    print(calculateSinCosTan(0))
    print(calculateSinCosTan(1))