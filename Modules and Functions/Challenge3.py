# ---------------------------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------------------------
#
#      Challenge 3: Compute & Return Maximum
# Implement 'findMaximum' function that receives two int arguments ' x' and 'y' and returns the maximum of the numbers.
#
#     Input: Two numbers x and y
#
#     Output: The maximum of two numbers x and y
#
#     Sample Input:
#         >x = 2, y = 3
#
#     Sample Output:
#         >3

def findMaximum(x, y):
    return max(x, y) # max() returns the maximum values

if __name__ == '__main__':
    print(findMaximum(2, 3))