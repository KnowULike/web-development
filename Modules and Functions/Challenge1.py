# ---------------------------------------------------------------------------------------------------
# Subject: Python                     ---------------------------------------------------------------
# Author: António Francisco C. Rocha  ---------------------------------------------------------------
# ---------------------------------------------------------------------------------------------------

    #   Challenge 1:
      # Write a findGCD function that takes in two numbers as input a and b and finds the greatest
      # common divisor of the two.

        # Input: Two numbers

        # Output: GCD of numbers

        # Sample Input:
            # a = 8, b = 12

        # Sample Output:
           # 4

import Challenge0       #Import my own module from previus challenge
import math             #Import built in 'math' Module

def findGCD(a, b):
    return math.gcd(a, b)

if __name__ == '__main__':
    print(Challenge0.returnString())
    print(findGCD(8, 12))