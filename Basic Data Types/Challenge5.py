"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

    Challenge 5: Find Index of a Specific Value in a String
Given a string, use a findOccurence(s) function that allows you to find the first occurrences of "b" and "ccc"
in the string.

    Input #A string
    
    Output #The first occurrence of “b” and “ccc” in the string
    
    Sample Input
        >aaabbbccc
    
    Sample Output
        >[3, 6]

"""
def findOccurence(s):
    b = s.index("b")
    c = s.index("c")
    return f"[{b},{c}]"

if __name__ == '__main__':
    print(findOccurence("aaabbbccc"))