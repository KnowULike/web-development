"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

    Challenge 2.:   Check Parity of a Number

Given a checkParity(n) function, write code to determine if a given number n is even or odd.
Think of this as a function that returns 0 if the number is even, and 1 if it is odd.

Input #A number

Output #The parity of the number

Sample Input # 4 

Sample Output # 0

"""
def checkParity(n):
    return n % 2

if __name__ == '__main__':
    print("Odd parity", checkParity(17))
    print("Even parity", checkParity(16))