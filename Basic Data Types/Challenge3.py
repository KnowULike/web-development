"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

    Challenge 3.:   Find Values Within a Range

Create inRange(x,y) function, write a method that determines whether a given pair (x, y) falls in the range
(x < 1/3 < y). Essentially, the function will be implementing the body of a function that takes in two numbers x and y
and returns True if x < 1/3 < y; otherwise, it returns False.

    Input #
        Two numbers, x and y
    
    Output #
        True if x and y are in the range and False otherwise.
    
    Sample Input #
        x = 2, y = 3
    
    Sample Output #
        True

"""
def inRange(x,y):
    return x < 1/3 < y

if __name__ == '__main__':
    print(inRange(1,2))
    print(inRange(-1, 2))