"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

    Challenge 4.:   String Transformation
Create a getStr() function, write the necessary sequence of operations to transform the string (containing three
literals) in such a way that every literal is tripled respectively.

    Input #A string
    
    Output #Triple of every string literal
    
    Sample Input
        >abc
    
    Sample Output 
        >aaabbbccc

"""
def getStr(input):
    output = ""
    for w in input:
        output += w * 3
    return output

if __name__ == '__main__':
    print(getStr("For exemple..."))
