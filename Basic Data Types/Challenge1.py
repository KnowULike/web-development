"""

---------------------------------------------------------------------------------------------------------------------
Subject: Python
Author: António Francisco C. Rocha
---------------------------------------------------------------------------------------------------------------------

""""""

    Challenge 1.:   Mathematical Calculations
Create a MathOp() function, try the following mathematical calculations and print the output:

    (3 / 2)
    (3 // 2)
    (3 % 2)
    (3**2)
    
    Output #
        >1.5
        >1
        >1
        >9

"""
def MathOp():
    classic_division = 3 / 2  ## Calculate 3/2 here
    floor_division = 3 // 2  ## Calculate 3//2 here
    modulus = 3 % 2  ## Calculate 3%2 here
    power = 3 ** 2  ## Calculate 3**2 here
    ## Returning the calculations for evaluation
    return [classic_division, floor_division, modulus, power]


if __name__ == '__main__':
    [op1, op2, op3, op4] = MathOp() #Assignment of operations to variables

    print(op1)
    print(op2)
    print(op3)
    print(op4)
